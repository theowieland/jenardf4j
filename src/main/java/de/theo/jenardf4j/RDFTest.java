package de.theo.jenardf4j;

import de.theo.jenardf4j.rdf.SerializerInterface;
import de.theo.jenardf4j.rdf.datamodel.Airplane;
import de.theo.jenardf4j.rdf.datamodel.ExampleDataModel;
import de.theo.jenardf4j.rdf.jena.JenaSerializer;
import de.theo.jenardf4j.rdf.rdf4j.RDF4JSerializer;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

/**
 * This project loads the given example input file into a java object model by using jena and rdf4j.
 */
public class RDFTest {

    public static void main(String[] args) {
        File inputFile = new File("input/rdf_example_data_model.ttl");

        try {
            RDFDataMgr.write(new FileOutputStream(inputFile), ExampleDataModel.createExamplePlaneModel(), Lang.TURTLE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //printing the example data model
        RDFDataMgr.write(System.out, ExampleDataModel.createExamplePlaneModel(), Lang.RDFXML);

        SerializerInterface jenaSerializer = new JenaSerializer();
        SerializerInterface rdf4jSerializer = new RDF4JSerializer();

        List<Airplane> jenaPlanes = jenaSerializer.read(inputFile);
        List<Airplane> rdf4jPlanes = rdf4jSerializer.read(inputFile);

        //print all planes as converted by the jena parser
        for (Airplane plane : jenaPlanes) {
            System.out.println("jena plane: " + plane);
        }

        //print all planes as converted by the rdf4j parser
        for (Airplane plane : rdf4jPlanes) {
            System.out.println("rdf4j plane: " + plane);
        }
    }
}
