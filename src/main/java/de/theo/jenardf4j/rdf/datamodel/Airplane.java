package de.theo.jenardf4j.rdf.datamodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class Airplane {

    private String uri;
    private long manufactureDate;
    private Person pilot;
    private List<Flight> flights;

    @Override
    public String toString() {
        return "Airplane{" +
                "uri='" + uri + '\'' +
                ", manufactureDate=" + manufactureDate +
                ", pilot=" + pilot +
                ", flights=" + flights +
                '}';
    }
}
