package de.theo.jenardf4j.rdf.datamodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class Flight {

    private String uri;
    private long departure;
    private long arrival;
    private List<Person> passengers;

    @Override
    public String toString() {
        return "Flight{" +
                "URI='" + uri + '\'' +
                ", departure=" + departure +
                ", arrival=" + arrival +
                ", passengers=" + passengers +
                '}';
    }
}
