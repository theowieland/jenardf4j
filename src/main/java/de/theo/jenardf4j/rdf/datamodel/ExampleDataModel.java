package de.theo.jenardf4j.rdf.datamodel;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import java.util.Calendar;
import java.util.Locale;

public class ExampleDataModel {

    private static final Model m = ModelFactory.createDefaultModel();

    public static final String DEFAULT_PREFIX = "http://url.de/planes/1.0#";

    public static final String PERSON_PREFIX = DEFAULT_PREFIX + "person/";
    public static final String FLIGHT_PREFIX = DEFAULT_PREFIX + "flight/";

    public static final Resource AIRPLANE_DATA_TYPE = m.createResource(DEFAULT_PREFIX + "types/Airplane", RDFS.Class);
    public static final Resource PERSON_DATA_TYPE = m.createResource(DEFAULT_PREFIX + "types/Person", RDFS.Class);
    public static final Resource FLIGHT_DATA_TYPE = m.createResource(DEFAULT_PREFIX + "types/Flight", RDFS.Class);

    public static final Property PILOT = m.createProperty(DEFAULT_PREFIX + "pilot");
    public static final Property MANUFACTURE_DATE = m.createProperty(DEFAULT_PREFIX + "manufactureDate");

    public static final Property NAME = m.createProperty(DEFAULT_PREFIX + "name");
    public static final Property AGE = m.createProperty(DEFAULT_PREFIX + "age");
    public static final Property BAGGAGE = m.createProperty(DEFAULT_PREFIX + "baggage");
    public static final Property ADDITIONAL_INFORMATION = m.createProperty(DEFAULT_PREFIX + "additionalInformation");

    public static final Property FLIGHTS_LIST = m.createProperty(DEFAULT_PREFIX + "flights_list");
    public static final Property FLIGHT_DEPARTURE = m.createProperty(DEFAULT_PREFIX + "flight/departure");
    public static final Property FLIGHT_ARRIVAL = m.createProperty(DEFAULT_PREFIX + "flight/arrival");
    public static final Property FLIGHT_PASSENGER_LIST = m.createProperty(DEFAULT_PREFIX + "flight/passengers");

    public static Model createExamplePlaneModel() {
        Model model = ModelFactory.createDefaultModel();

        model.setNsPrefix("example", DEFAULT_PREFIX);

        Resource pilot = model.createResource(PERSON_PREFIX + "pilot-person-id1")
                .addProperty(RDF.type, PERSON_DATA_TYPE)
                .addLiteral(NAME, "Robert")
                .addLiteral(AGE, 46)
                .addLiteral(BAGGAGE, true);

        Resource person1 = model.createResource(PERSON_PREFIX + "passenger-person-id1")
                .addProperty(RDF.type, PERSON_DATA_TYPE)
                .addLiteral(NAME, "Hans")
                .addLiteral(AGE, 42)
                .addLiteral(BAGGAGE, false)
                .addLiteral(ADDITIONAL_INFORMATION, model.createLiteral("Dieser String enthält weitere Informationen.", Locale.GERMAN.toLanguageTag()))
                .addLiteral(ADDITIONAL_INFORMATION, model.createLiteral("This string contains further information.", Locale.ENGLISH.toLanguageTag()));

        Resource person2 = model.createResource(PERSON_PREFIX + "passenger-person-id2")
                .addProperty(RDF.type, PERSON_DATA_TYPE)
                .addLiteral(NAME, "Julia")
                .addLiteral(AGE, 87)
                .addLiteral(BAGGAGE, true);

        Resource flight1 = model.createResource(FLIGHT_PREFIX + "flight-id1")
                .addProperty(RDF.type, FLIGHT_DATA_TYPE)
                .addLiteral(FLIGHT_ARRIVAL, model.createTypedLiteral(Calendar.getInstance()))
                .addLiteral(FLIGHT_DEPARTURE, model.createTypedLiteral(Calendar.getInstance()))
                .addProperty(FLIGHT_PASSENGER_LIST, model.createList(new RDFNode[] {person1, person2}));

        Resource flight2 = model.createResource(FLIGHT_PREFIX + "flight-id2")
                .addProperty(RDF.type, FLIGHT_DATA_TYPE)
                .addLiteral(FLIGHT_ARRIVAL, model.createTypedLiteral(Calendar.getInstance()))
                .addLiteral(FLIGHT_DEPARTURE, model.createTypedLiteral(Calendar.getInstance()))
                .addProperty(FLIGHT_PASSENGER_LIST, model.createList(new RDFNode[] {person1, person2}));

        Resource plane = model.createResource(DEFAULT_PREFIX + "plane/plane-id1")
                .addProperty(RDF.type, AIRPLANE_DATA_TYPE)
                .addProperty(PILOT, pilot)
                .addLiteral(MANUFACTURE_DATE, model.createTypedLiteral(new XSDDateTime(Calendar.getInstance()), XSDDatatype.XSDdateTime))
                .addProperty(FLIGHTS_LIST, model.createList(new RDFNode[] {flight1, flight2}));

        return model;
    }
}
