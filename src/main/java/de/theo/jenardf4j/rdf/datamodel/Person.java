package de.theo.jenardf4j.rdf.datamodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class Person {

    private String uri;
    private String name;
    private int age;
    private boolean hasBaggage;
    private List<LanguageString> additionalInformation;

    @Override
    public String toString() {
        return "Person{" +
                "uri='" + uri + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", hasBaggage=" + hasBaggage +
                ", additionalInformation=" + additionalInformation +
                '}';
    }
}
