package de.theo.jenardf4j.rdf.datamodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Locale;

@AllArgsConstructor
@Getter
@Setter
public class LanguageString {

    private String message;
    private Locale locale;

    @Override
    public String toString() {
        return "LanguageString{" +
                "message='" + message + '\'' +
                ", locale=" + locale +
                '}';
    }
}
