package de.theo.jenardf4j.rdf;

import java.io.File;
import java.util.List;

public interface SerializerInterface<T> {

    List<T> read(File input);
}
