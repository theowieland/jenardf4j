package de.theo.jenardf4j.rdf.jena;

import de.theo.jenardf4j.rdf.datamodel.ExampleDataModel;
import de.theo.jenardf4j.rdf.datamodel.Flight;
import de.theo.jenardf4j.rdf.datamodel.Person;
import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import java.util.ArrayList;
import java.util.List;

public class JenaFlightParser {

    public static Flight parse(Model model, Resource resource) {
        String uri = resource.getURI();
        XSDDateTime departureDate = (XSDDateTime) resource.getProperty(ExampleDataModel.FLIGHT_DEPARTURE).getLiteral().getValue();
        XSDDateTime arrivalDate = (XSDDateTime) resource.getProperty(ExampleDataModel.FLIGHT_ARRIVAL).getLiteral().getValue();

        List<RDFNode> passengersList = resource.getProperty(ExampleDataModel.FLIGHT_PASSENGER_LIST).getResource().as(RDFList.class).asJavaList();
        List<Person> passengers = new ArrayList<>();
        for (RDFNode passengerNode : passengersList) {
            passengers.add(JenaPersonParser.parse(model, model.getResource(passengerNode.asResource().getURI())));
        }

        return new Flight(uri, departureDate.asCalendar().getTimeInMillis(), arrivalDate.asCalendar().getTimeInMillis(), passengers);
    }
}
