package de.theo.jenardf4j.rdf.jena;

import de.theo.jenardf4j.rdf.SerializerInterface;
import de.theo.jenardf4j.rdf.datamodel.Airplane;
import de.theo.jenardf4j.rdf.datamodel.ExampleDataModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.RDF;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class JenaSerializer implements SerializerInterface<Airplane> {

    @Override
    public List<Airplane> read(File input) {
        Model model = RDFDataMgr.loadModel(input.getAbsolutePath());
        StmtIterator resourcesToParse = model.listStatements(null, RDF.type, ExampleDataModel.AIRPLANE_DATA_TYPE); //filter all resources with the given datatype

        List<Airplane> parsedPlanes = new ArrayList<>();
        while (resourcesToParse.hasNext()) { //iterate over all airplane resources in the given input file
            Statement statement = resourcesToParse.next();

            Airplane result = JenaAirplaneParser.parse(model, model.getResource(statement.getSubject().getURI()));
            parsedPlanes.add(result);
        }

        return parsedPlanes;
    }
}
