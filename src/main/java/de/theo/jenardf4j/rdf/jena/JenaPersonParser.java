package de.theo.jenardf4j.rdf.jena;

import de.theo.jenardf4j.rdf.datamodel.ExampleDataModel;
import de.theo.jenardf4j.rdf.datamodel.LanguageString;
import de.theo.jenardf4j.rdf.datamodel.Person;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class JenaPersonParser {

    public static Person parse(Model model, Resource resource) {
        String uri = resource.getURI();
        String name = resource.getProperty(ExampleDataModel.NAME).getString();
        int age = resource.getProperty(ExampleDataModel.AGE).getInt();
        boolean baggage = resource.getProperty(ExampleDataModel.BAGGAGE).getBoolean();

        List<LanguageString> additionalInformation = new ArrayList<>();
        if (resource.hasProperty(ExampleDataModel.ADDITIONAL_INFORMATION)) {
            StmtIterator additionalInformationIterator = resource.listProperties(ExampleDataModel.ADDITIONAL_INFORMATION);
            while (additionalInformationIterator.hasNext()) {
                Statement additionalString = additionalInformationIterator.next();
                additionalInformation.add(new LanguageString(additionalString.getString(), Locale.forLanguageTag(additionalString.getLanguage())));
            }
        }

        return new Person(uri, name, age, baggage, additionalInformation);
    }
}
