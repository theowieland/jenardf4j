package de.theo.jenardf4j.rdf.jena;

import de.theo.jenardf4j.rdf.datamodel.Airplane;
import de.theo.jenardf4j.rdf.datamodel.ExampleDataModel;
import de.theo.jenardf4j.rdf.datamodel.Flight;
import de.theo.jenardf4j.rdf.datamodel.Person;
import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import java.util.ArrayList;
import java.util.List;


public class JenaAirplaneParser {

    public static Airplane parse(Model model, Resource resource) {
        String uri = resource.getURI();
        XSDDateTime manufactureDate = (XSDDateTime) resource.getProperty(ExampleDataModel.MANUFACTURE_DATE).getLiteral().getValue();
        Person pilot = JenaPersonParser.parse(model, model.getResource(resource.getProperty(ExampleDataModel.PILOT).getResource().getURI()));

        List<RDFNode> flightsList = resource.getProperty(ExampleDataModel.FLIGHTS_LIST).getResource().as(RDFList.class).asJavaList();
        List<Flight> flights = new ArrayList<>();
        for (RDFNode flightNode : flightsList) {
            flights.add(JenaFlightParser.parse(model, model.getResource(flightNode.asResource().getURI())));
        }

        return new Airplane(uri, manufactureDate.asCalendar().getTimeInMillis(), pilot, flights);
    }
}
