package de.theo.jenardf4j.rdf.rdf4j;

import de.theo.jenardf4j.rdf.SerializerInterface;
import de.theo.jenardf4j.rdf.datamodel.Airplane;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RDF4JSerializer implements SerializerInterface<Airplane> {

    @Override
    public List<Airplane> read(File input) {
        Model model = null;
        try {
            model = Rio.parse(new FileInputStream(input), RDFFormat.TURTLE);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ValueFactory vf = SimpleValueFactory.getInstance();

        List<Airplane> parsedPlanes = new ArrayList<>();
        for (Resource plane : model.filter(null, RDF.TYPE, RDF4JPlaneConstants.AIRPLANE_DATA_TYPE).subjects()) { //iterate over all airplanes in the given model
            parsedPlanes.add(RDF4JAirplaneParser.parse(model, plane));
        }

        return parsedPlanes;
    }
}
