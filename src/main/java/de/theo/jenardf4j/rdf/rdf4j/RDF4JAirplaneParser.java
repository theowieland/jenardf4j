package de.theo.jenardf4j.rdf.rdf4j;

import de.theo.jenardf4j.rdf.datamodel.Airplane;
import de.theo.jenardf4j.rdf.datamodel.Flight;
import de.theo.jenardf4j.rdf.datamodel.Person;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.RDFCollections;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;

import static org.eclipse.rdf4j.model.util.Values.iri;

public class RDF4JAirplaneParser {

    public static Airplane parse(Model model, Resource resource) {
        String uri = resource.toString();

        XMLGregorianCalendar manufactureCalendar = Models.objectLiteral(model.filter(resource, RDF4JPlaneConstants.MANUFACTURE_DATE, null)).get().calendarValue();

        String pilotIri = Models.objectString(model.filter(resource, RDF4JPlaneConstants.PILOT, null)).get();
        Person pilot = RDF4JPersonParser.parse(model, model.filter(iri(pilotIri), null, null).subjects().iterator().next());

        Resource flightsNode = Models.objectResource(model.filter(resource, RDF4JPlaneConstants.FLIGHTS_LIST, null)).get();
        List<Value> flightsValues = RDFCollections.asValues(model, flightsNode, new ArrayList<>());
        ArrayList<Flight> flights = new ArrayList<>();
        for (Value value : flightsValues) {
            flights.add(RDF4JFlightParser.parse(model, model.filter(iri(value.toString()), null, null).subjects().iterator().next()));
        }

        return new Airplane(uri, manufactureCalendar.toGregorianCalendar().getTimeInMillis(), pilot, flights);
    }
}
