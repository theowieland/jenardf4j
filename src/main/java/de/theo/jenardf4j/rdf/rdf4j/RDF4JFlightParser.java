package de.theo.jenardf4j.rdf.rdf4j;

import de.theo.jenardf4j.rdf.datamodel.Flight;
import de.theo.jenardf4j.rdf.datamodel.Person;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.util.Models;
import org.eclipse.rdf4j.model.util.RDFCollections;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;

import static org.eclipse.rdf4j.model.util.Values.iri;

public class RDF4JFlightParser {

    public static Flight parse(Model model, Resource resource) {
        String uri = resource.toString();
        XMLGregorianCalendar departure = Models.objectLiteral(model.filter(resource, RDF4JPlaneConstants.FLIGHT_DEPARTURE, null)).get().calendarValue();
        XMLGregorianCalendar arrival = Models.objectLiteral(model.filter(resource, RDF4JPlaneConstants.FLIGHT_ARRIVAL, null)).get().calendarValue();

        Resource passengersNode = Models.objectResource(model.filter(resource, RDF4JPlaneConstants.FLIGHT_PASSENGER_LIST, null)).get();
        List<Value> passengersValue = RDFCollections.asValues(model, passengersNode, new ArrayList<>());
        List<Person> passengers = new ArrayList<>();
        for (Value value : passengersValue) {
            passengers.add(RDF4JPersonParser.parse(model, model.filter(iri(value.toString()), null, null).subjects().iterator().next()));
        }

        return new Flight(uri, departure.toGregorianCalendar().getTimeInMillis(), arrival.toGregorianCalendar().getTimeInMillis(), passengers);
    }
}
