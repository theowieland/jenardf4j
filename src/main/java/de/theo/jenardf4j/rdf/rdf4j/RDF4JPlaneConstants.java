package de.theo.jenardf4j.rdf.rdf4j;

import org.eclipse.rdf4j.model.IRI;

import static org.eclipse.rdf4j.model.util.Values.iri;

public class RDF4JPlaneConstants {

    private static final String NAMESPACE = "http://url.de/planes/1.0#";

    public static final IRI AIRPLANE_DATA_TYPE = iri(NAMESPACE + "types/Airplane");

    public static final IRI MANUFACTURE_DATE = iri(NAMESPACE, "manufactureDate");

    public static final IRI PILOT = iri(NAMESPACE, "pilot");
    public static final IRI NAME = iri(NAMESPACE + "name");
    public static final IRI AGE = iri(NAMESPACE + "age");
    public static final IRI BAGGAGE = iri(NAMESPACE + "baggage");
    public static final IRI ADDITIONAL_INFORMATION = iri(NAMESPACE + "additionalInformation");

    public static final IRI FLIGHTS_LIST = iri(NAMESPACE, "flights_list");
    public static final IRI FLIGHT_DEPARTURE = iri(NAMESPACE, "flight/departure");
    public static final IRI FLIGHT_ARRIVAL = iri(NAMESPACE, "flight/arrival");
    public static final IRI FLIGHT_PASSENGER_LIST = iri(NAMESPACE, "flight/passengers");
}
