package de.theo.jenardf4j.rdf.rdf4j;

import de.theo.jenardf4j.rdf.datamodel.LanguageString;
import de.theo.jenardf4j.rdf.datamodel.Person;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.util.Models;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RDF4JPersonParser {

    public static Person parse(Model model, Resource resource) {
        String uri = resource.toString();
        String name = Models.objectString(model.filter(resource, RDF4JPlaneConstants.NAME, null)).get();
        int age = Models.objectLiteral(model.filter(resource, RDF4JPlaneConstants.AGE, null)).get().intValue();
        boolean baggage = Models.objectLiteral(model.filter(resource, RDF4JPlaneConstants.BAGGAGE, null)).get().booleanValue();

        List<LanguageString> additionalInformation = new ArrayList<>();
        for (Value information : model.filter(resource, RDF4JPlaneConstants.ADDITIONAL_INFORMATION, null).objects()) {
            additionalInformation.add(new LanguageString(information.stringValue(), Locale.forLanguageTag(((Literal) information).getLanguage().get())));
        }

        return new Person(uri, name, age, baggage, additionalInformation);
    }
}
